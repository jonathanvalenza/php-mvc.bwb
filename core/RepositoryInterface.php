<?php
/**
 * préparation au class nécéssaire au future DAO. cette class de type interface servira de model lors 
 * de la création des différent DAO que nous allons créer
 */ 
interface RepositoryInterface {
    /*
    * methode de recupération des données. Cette methode va permetre de récupérer tous les elements de la ou les tables
    */
    public function getAll(): array;

    /*
    * methode de recupération des données. Cette methode va permetre de récupérer tous les elements de la ou les tables
    * en fonction des filtres entré en arguments (ex: toutes les donnée de la table avec l'id 5, le nom "toto" etc...)
    */
    public function getAllBy(array $filters): array;


    
}