<?php

/**
 * création de la class DAO qui a comme parent les class abstraite CRUDInterface et RepositoryInterface. 
 * cette class servira de parent au autre class DAO (ex: DAOUser, DAOLivre...)
 */ 
abstract class DAO implements CRUDInterface, RepositoryInterface {
    
    /*
     * C'est dans cette attribut que nous allons stoquer les nouveaux PDO
     */
    protected $pdo;

    /*
     * Lors de la création d'une class de type DAO , l'attribut pdo se vera attribuer un nouveau PDO. 
     */
    public function __construct() {
        $config = json_decode(file_get_contents($_SERVER["DOCUMENT_ROOT"] . "config/database.json"), true);
        $dsn = $config["driver"] . ":dbname=" . $config["dbname"] . ";host=" . $config["host"] . ";charset=" . $config["charset"];
        $this->pdo = new PDO($dsn, $config["username"], $config["password"]);
        
    }
}