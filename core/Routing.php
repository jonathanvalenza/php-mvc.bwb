<?php

class Routing {

    // lors que la création de la nouvel classe, on stock routing.json dans l'attribut $config.
    private $config; 

    // $uri stock le resultat du découpage de l'URI en tableau
    private $uri;

    // $route stock le resltat du découpage de routing.json (stocké dans $config) en tableau
    private $route;

    // la chaine $controller corespond au controller aui a été trouvé
    private $controleur;

    // la liste $args est un tableau des arguments a passer au controlleur, qui représente
    // les éléments variabe de l'URI
    private $args;

    //la chaine $method correspond au verbe http utiliser lors de la requete
    private $method;

    /**
     * Le constructeur va initialisé la propriété config en la peuplant 
     * des données contenue dans le fichier config/routing.json 
     * json_decode() = transforme un format json en chaine de caractere.
     * file_get_content() = Lis tout un fichier et le stock dans une chaîne.
     */
    public function __construct() {
        // Je lit le fichier de config et le transforme en chaine de caractere
        // puis je convertit la chaine(json) en tableau associatif
        $this->config = json_decode(file_get_contents($_SERVER["DOCUMENT_ROOT"] . "/config/routing.json"), true);
        
        // On signifie que args est un tableau
        $this->args = array();

        // On découpe l'uri et on en fait un tableau grace à explode (le 1ier argument d'explode désigne le caractere qui servira 
        // a diviser la chaine de caractere et le 2ieme argument est l'uri récupéré grace a la super global $_SERVER["REQUEST_URI])
        // puis on l'affecte a URI.
        $this->uri = explode("/",$_SERVER["REQUEST_URI"]);

        // on affecte le verbe http (récupéré avec la super global $_SERVER["REQUEST_METHOD]) dans l'attribut method
        $this->method = $_SERVER["REQUEST_METHOD"];
    }

    /**
     * cette methode sert a invoquer les autres methode. A chaque requete http, cette methode 
     * va déclencher la suite des evènements. Au final, elle va apeller la methode demandé dans le DAO 
     * (ex: create, getAll...)
     */
    public function execute() {
        //Dans le config, les différentes methode de DAO sont référencé, nous devons donc checker
        //chaque entré de config et les comparer
        foreach($this->config as $key=>$value) {

            //On affecte a $route chaque clé du tableau config 
            //("/" sert a délimité les entré, par exemple, /api/users, api sera une entré et users une autre entré)
            $this->route = explode("/",$key);

            // on verifie si les tableau sont de même taille avec isEqual()
            if ($this->isEqual()) {
                // s'il sont de la même taille, on vérifie s'ils sont identique avec compare()
                if ($this->compare()) {
                    // s'il sont identique on utilise getValue() et on affect le resultat dans controller
                    $this->controleur = $this->getValue($value);
                    // puis on invoque la method invok qui va lancer l'action adequate dans le controler
                    $this->invoke();
                }
            }

        }
    }

    /**
     * teste si la longueur du tableau de l'uri est egale à l'une des différentes longueur des URI du routing.json
     * exemple URI = user/message (donc egale 2) et uri sur le json = /api/users (donc egale 2), les 2 sont de même longueur
     * (même si elle ne sont pas identique)
     */
    private function isEqual() {
        if (count($this->uri) == count($this->route)) { // Si c'est egale, revoie true
            return true;
        } else { // si non, ça renvoie false
            return false;
        }
    }

    /**
     * si isEqual retourne vrai, alors on compare si les deux résultat sont identique (route et URI). 
     * si les 2 corresponde, c'est que le route a été trouvé.
     */
    private function compare() {
        for ($i = 0; $i <= count($this->uri)-1; $i++) { // on va checker chaque entré des tableau
            if ($this->uri[$i] != $this->route[$i]) { // si les 2 valeur sont différent, on teste si la différence est un argument
                if ($this->route[$i] == "(:)") { // si la valeur de route est (:), c'est que la valeur est une variable
                    $this->addArgument($i); // on affecte la variable dans l'attribut args grace à addArgument()
                } else {
                return false; // si ce n'est pas une variable, c'est que l'uri et la route sont différent, donc on sort et retourn false
                }
            }
        }
        return true; // si la boucle se termine, c'est que l'uri et la route sont identique, donc on retourne true
    }

    /**
     * si compare() trouve 2 valeurs identique, getValue retourne la valeur en question 
     * (la valeur est le controlleur correspondant a la route selectionné)
     */
    private function getValue($value) { 
        // si la valeur passé en argument est une tableau, on retourne la valeur associé à la clé du verbe http 
        // stocké dans l'attribut method
        if (is_array($value)) { 
            return $value[$this->method];
        } // si ce n'est pas un tableau, on retourne $value
        return $value;
    }

    /**
     * cette methode ajoute l'élément variable de l'URI si l'élément en cour est sencé être variable
     */
    private function addArgument($index) {
            array_push($this->args, $this->uri[$index]);
    }

    /**
     * cette methode est invoqué quand le controller a été selectionné. elle créée un objet controller 
     * et invoque la methode en y passant les arguments adequate
     */
    private function invoke() {
        // on divise la valeur stocké dans l'attribut controller et on le stock dans un tableau grace a explode
        // et on affecte le tableau dans une variable. 
        // la clé 0 contient le nom de la class et la clé 1 contient la method a apeller
        $explodeControl = explode(":", $this->controleur);
        // on crée un nouvel objet de la class qu'on a stocké dans le tableau qui a été affecté 
        // dans la variable $explodeControl a l'index 0
        $DAOUser = new $explodeControl[0]();
        // on utilise call_user_func-array pour lancer le controller avec les methods entré en argument
        // call_user_func_array(array([objet], [method]), array([argument]))
        call_user_func_array(array($DAOUser, $explodeControl[1]), array($this->args));
    }
}

