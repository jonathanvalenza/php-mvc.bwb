<?php
/**
 * préparation au class DAO concrète (ex : DOAUser) avec l'implementation du CRUD. cette 
 * class de type interface servira de model lors de la création des différent DAO 
 * que nous allons créer
 */ 
interface CRUDInterface {
    /*
    * methode de recupération des données. Cette methode va permetre de récupérer les données dont l'id a été
    * passé en argument.
    */
    public function retrieve(int $id);

    /* 
     * methode de mise à jour des données. Cette methode va permetre de changer les données dans le tableau qui a été
     * passé en argument.
     */ 
    public function update(array $data);

    /**
     * methode de supression des données. Cette methode va permetre de suprimer les données dont l'id a été
     * passé en argument.
     */
    public function delete(int $id);

    /**
     * methode création des données. Cette methode va permetre de crééer des données dans le tableau qui a été
     * passé en argument.
     */
    public function create(array $data);
    
}